/***********************************************************************
* GPU Lisa: Image Reconstruction on the GPU
* Copyright (C) 2011 Andrew Thomas Ortman <andrew.ortman@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/

#include "rectangle.h"

//returns true if a point (x,y) is in bounds of the rectangle (optimized for host)
bool ml_rectangle_pointinbounds(ml_rectangle_t* rectangle, int x, int y)
{
	return (x > rectangle->x && x < (rectangle->x + rectangle->width)) && (y > rectangle->y && y < (rectangle->y + rectangle->height));
}

void ml_rectangle_randomize(unsigned int* random_seed, ml_rectangle_t* rectangle, int max_width, int max_height)
{
		int x = RAND_RI_RANGE(random_seed, 0, max_width-1);
		int y = RAND_RI_RANGE(random_seed, 0, max_height-1);
		
		rectangle->x = x;
		rectangle->y = y;
		rectangle->width = RAND_RI_RANGE(random_seed, 1, max_width-x); 
		rectangle->height = RAND_RI_RANGE(random_seed, 1, max_height-y);
		
		ml_color_alpha_randomize(random_seed, &(rectangle->color_info));
}