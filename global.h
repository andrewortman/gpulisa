/***********************************************************************
* GPU Lisa: Image Reconstruction on the GPU
* Copyright (C) 2011 Andrew Thomas Ortman <andrew.ortman@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/

#ifndef ML_GLOBAL
#define ML_GLOBAL

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <signal.h>


//the maximum number of rectangles a reconstruction can have
#define MAX_RECTANGLES 150

#ifdef __MLDEBUG__
#define DEBUG(message,...) \
	printf("DEBUG %s [%s:%d] => ", __FUNCTION__, __FILE__, __LINE__); \
	printf(message, ##__VA_ARGS__); \
	printf("\r\n");

#define HANDLE_CUDA_ERROR(A) if(A != cudaSuccess) { DEBUG("CUDA ERROR: %i: %s", A, cudaGetErrorString(A)); exit(A); }
#else

#define DEBUG(message, ...)
#define HANDLE_CUDA_ERROR(A) if(A != cudaSuccess) { printf("An error occured with the GPU. Error code: %i: %s\r\n", A, cudaGetErrorString(A)); exit(A); }

#endif

#define bool int
#define true 1
#define false 0

//some random number helpers

//generates a random float from 0 to 1.0
#define RAND_RF(A) rand_r(A)/(float)RAND_MAX

//generates a random float from B->C
#define RAND_RF_RANGE(A,B,C) (((float)(C-B)*(rand_r(A)/(float)RAND_MAX))+(float)B)

//generates a random int from 0->RAND_MAX
#define RAND_RI(A) rand_r(A)

//generates a random int from B->C (C is noninclusive)
#define RAND_RI_RANGE(A,B,C) ((C-B)==0? C : (int)(rand_r(A)%(C-B) + B))

//generates a boolean with a chance of true being B (a float)
#define RAND_RB_CHANCE(A,B) ((float)(rand_r(A)/(float)RAND_MAX) < B)

//settings struct
typedef struct 
{
	bool  fixed_number_of_rectangles;
	float add_rectangle_chance;
	float remove_rectangle_chance;
	float swap_rectangles_chance;
	
	float change_color_chance;
	float change_transparency_chance;
	float change_size_chance;
	float change_position_chance;
	
	int position_max_change;
	int size_max_change;
	float color_max_change;
	float transparency_max_change;
	
	int population_size;
	int elite_population_size;
	
} ml_settings_t;

extern ml_settings_t global_settings; //defined in main.c
#endif
