/***********************************************************************
* GPU Lisa: Image Reconstruction on the GPU
* Copyright (C) 2011 Andrew Thomas Ortman <andrew.ortman@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/

#include "global.h"
#include "rendered_image.h"
#include "recon_image.h"
#include "png_io.h"
#include "genetic_operations.h"
#include "cuda.h"
#include <time.h>

ml_settings_t global_settings = 
{
	.fixed_number_of_rectangles = true,
	.add_rectangle_chance = 0.01f, //doesnt matter if fixed number
	.remove_rectangle_chance = 0.01f, //doesnt matter if fixed number
	.swap_rectangles_chance = 0.05f,
	
	.change_color_chance = 0.025f,
	.change_transparency_chance = 0.025f,
	.change_size_chance = 0.025f,
	.change_position_chance = 0.025f,

	.position_max_change = 20,
	.size_max_change = 20,
	.color_max_change = 0.3f,
	.transparency_max_change = 0.3f,
	
	.population_size = 500,
	.elite_population_size = 10
	
};

//Execution kernel
extern void executeFitnessKernel(ml_rendered_im_t* target_device, ml_recon_im_t** recon_list_device, ml_rendered_im_t** rendered_list_device, int size, float* output_host);

int ml_roullete_wheel_selection(unsigned int* random_seed, float* fitnesses, float fitness_sum);

int main(int argc, char** argv)
{
	if(argc != 2)
	{
		printf("Usage: ./gpulisa /path/to/input.png\r\n");
		return 1;
	}
	
	printf("Loading %s... ", argv[1]);
	ml_rendered_im_t* target = ml_png_load(argv[1]);
	
	if(target == NULL)
	{
		printf("Error: invalid PNG file.\r\n");
		return 2;
	}
	
	
	printf("Complete!\r\n");
	DEBUG("Loaded PNG: %p", target);
	
	printf("Initializing CUDA.. ");
	
	//check to see if we have more than 1 device avaliable
	int device_count = 0;
	cudaGetDeviceCount(&device_count);
	printf("Error: Found %i CUDA-enabled devices.\r\n", device_count);
	
	if(device_count == 0)
	{
		printf("1 device is required to run.\r\n");
		return 3;
	}
	
	
	//create the first population randomly
	printf("Creating random population.. ");
	unsigned int random_seed = (unsigned int)time(NULL);
	ml_recon_im_t* population = (ml_recon_im_t*)malloc(sizeof(ml_recon_im_t)*global_settings.population_size);
	ml_recon_im_t* new_population = (ml_recon_im_t*)malloc(sizeof(ml_recon_im_t)*global_settings.population_size);
	
	for(int i = 0; i < global_settings.population_size; i++)
	{
		ml_reconimage_randomize(&random_seed, &(population[i]), target->width, target->height);
	}
	printf("Generated!\r\n");
	
	//create buffer on the device for rendered outputs
	ml_rendered_im_t** rendered_output_list_device;
	ml_rendered_im_t** rendered_output_list_host;
	ml_renderedimage_allocatearraydevice(&rendered_output_list_host, &rendered_output_list_device, target->width, target->height, global_settings.population_size);
	
	//create buffer on the device for recon outputs
	ml_recon_im_t** recon_input_list_device;
	ml_recon_im_t** recon_input_list_host;
	ml_reconimage_allocatearraydevice(&recon_input_list_host, &recon_input_list_device, global_settings.population_size);
	
	ml_rendered_im_t* target_device = ml_renderedimage_uploadtodevice(target);
	int generation_number = 0;
	float* fitness_outputs = (float*)malloc(sizeof(float)*global_settings.population_size);
	
	ml_rendered_im_t* best_member_tmp = ml_renderedimage_create(target->width, target->height);
	
	while(true)
	{
		printf("Computing population %i\r\n",++generation_number);
		
		cudaEvent_t start_mem_cpy, stop_mem_cpy;
		cudaEventCreate(&start_mem_cpy); cudaEventCreate(&stop_mem_cpy);
		//STEP ONE: Upload the reconstruction image information the GPU
		printf("\t 1) Uploading population to GPU.. ");
		cudaEventRecord(start_mem_cpy,0);
		ml_reconimage_uploadarraytodevice(recon_input_list_host, population, global_settings.population_size);
		cudaEventRecord(stop_mem_cpy,0);
		cudaEventSynchronize(stop_mem_cpy);
		float memcpytime = 0.0f;
		cudaEventElapsedTime(&memcpytime, start_mem_cpy, stop_mem_cpy);
		printf("Complete. Took %f ms to copy.\r\n",memcpytime);
		
		
		//STEP TWO: Calculate the fitnesses of each member of the population
		printf("\t 2) Executing fitness kernel.. ");
		
		cudaEvent_t start_exec, stop_exec;
		cudaEventCreate(&start_exec); cudaEventCreate(&stop_exec);
		cudaEventRecord(start_exec,0);

		//download the fitness results into 'fitness_outputs' array
		executeFitnessKernel(target_device, recon_input_list_device, rendered_output_list_device, global_settings.population_size, fitness_outputs);
		
		cudaEventRecord(stop_exec,0);
		cudaEventSynchronize(stop_exec);
		float exec_time = 0.0f;
		cudaEventElapsedTime(&exec_time, start_exec, stop_exec);

		printf("Complete. Took %f ms to compute. ", exec_time);
		
		
		float best_fitness = 0.0f; //the best fitness
		int best_fitness_index = 0; //the index of the best fitness
		float fitness_sum = 0.0f; //stores the total sum of all of the fitness functions (for roullete wheel selection)
		for(int i = 0; i < global_settings.population_size; i++)
		{
			fitness_sum+=fitness_outputs[i];
			if(best_fitness < fitness_outputs[i])
			{
				best_fitness = fitness_outputs[i];
				best_fitness_index = i;
			}
			//printf("Fitness %i = %f\r\n", i, fitness_outputs[i]);
		}

		printf("Best fitness %0.5f (member %i)\r\n", best_fitness, best_fitness_index);

	
		printf("\t 3) Downloading rendered image.. ");
		
		if(best_fitness_index == 0 && generation_number % 100 != 0)
		{ 
			printf("Skipping.\r\n");
		}
		else
		{
			int i = 0;
			//for(int i = 0; i < global_settings.population_size; i++)
			//{
			ml_renderedimage_downloadfromdevice(best_member_tmp, rendered_output_list_host[i]);
			//printf("Saving to disk.. ");
		
			char file_location[100];
			sprintf(file_location, "outputs/%08i-%i.png", generation_number,i);
			ml_png_save(file_location, best_member_tmp);
			//printf("done!\r\n");
			//}
			printf("done!\r\n");
		}
		
		
		clock_t start_generate_time = clock();
		printf("\t 4) Creating new generation.. ");
		
		//place the best of the best ("the elites") into the population 
		if(global_settings.elite_population_size > 0) //if we have an elite population, copy them in
		{
			//we are going to go ahead and put the best in the list (this is why we check)
			new_population[0] = population[best_fitness_index];
			
			//we dont want to put duplicates into the elite pool, so keep a list of who we put in already
			int* elite_selection_indices = (int*)malloc(sizeof(int)*global_settings.elite_population_size);
			elite_selection_indices[0] = best_fitness_index;
			
			//copy the best of the best into the new generation
			float last_best_fitness = best_fitness; 
			float current_best_fitness;
			int current_best_fitness_index;
			
			for(int i = 1; i < global_settings.elite_population_size; i++) 
			{   
				//i contains the position in the new population we put the next elite in
				
				current_best_fitness = 0.0f;
				current_best_fitness_index = -1;
				for(int x = 0; x < global_settings.population_size; x++)
				{
					//x contains the current member of the previous population who we are checking for elite candidacy
					
					if(fitness_outputs[x] <= last_best_fitness && fitness_outputs[x] >= current_best_fitness)
					{
						//possible elite candidate
						//so check to see if this member is already in our elite pool
						bool contains_member = false;
						for(int y = 0; y < i; y++)
						{
							//y contains the index of the current elite pool index list
							if(x == elite_selection_indices[y])
							{
								contains_member = true;
								break;
							}
						}
						
						//isnt in the pool, so lets make it the best candidate so far
						if(!contains_member)
						{
							current_best_fitness_index = x;
							current_best_fitness = fitness_outputs[x];
						}
					}
				}
				
				if(current_best_fitness_index == -1)
				{
					printf("Error: Ran out of members to select as elites. This shouldn't happen!\r\n");
					return 100;
				}
				elite_selection_indices[i] = current_best_fitness_index;
				new_population[i] = population[current_best_fitness_index];
				last_best_fitness = current_best_fitness;
			}
			
			free(elite_selection_indices); //get rid of that allocated array of elite selections
		} //check to see if we had an elite population
		
		//generate new non-elite members of the population
		for(int i = global_settings.elite_population_size; i < global_settings.population_size; i++)
		{
			//i contains the point in the population to put the new member
			
			//pick a random parent
			ml_recon_im_t child;
			int parent_index = ml_roullete_wheel_selection(&random_seed, fitness_outputs, fitness_sum);
			
			//create a new child by mutating the parent
			child = population[parent_index];
			ml_geneticops_mutate(&random_seed, &child, target->width, target->height);
			
			//and add it into the list
			new_population[i] = child;
		}
		
		float cpu_generate_time = 1000*(clock()-start_generate_time)/(float)CLOCKS_PER_SEC;
		printf("done! Took %f ms to generate on CPU.\r\n", cpu_generate_time);
		
		//swap the new generation with the previous one and continue on
		ml_recon_im_t* swap = population;
		population = new_population;
		new_population = swap;
		
	} //Repeat the processing on the new population
	
}

//choose a spot with selection probability proportional to fitness
//this is known as "roullete-wheel" selection or "Fitness proportionate" selection
//if restricted_index is provided, will keep selecting until an index that is not equal to the restricted_index
//is chosen

//last parameter is optional, see prototype at the top of this file
int ml_roullete_wheel_selection(unsigned int* random_seed, float* fitnesses, float fitness_sum) 
{	
	int index = 0;
	float fitness_cumulative_sum; //stores the rolling sum during selection
	fitness_cumulative_sum = 0.0f;
	float fitness_point = RAND_RF_RANGE(random_seed,0.0f,fitness_sum); //pick a random spot in our range
	for(int i = 0; i < global_settings.population_size; i++)
	{
	
		fitness_cumulative_sum += fitnesses[i];
		
		//Check to see if we crosssed over the point where the sum is now greater than our chosen point
		if(fitness_point < fitness_cumulative_sum) 
		{
			//we hit the point, record the index and move on
			index = i;
			break;
		}
	}
	
	
	return index;
}
