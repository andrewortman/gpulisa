/***********************************************************************
* GPU Lisa: Image Reconstruction on the GPU
* Copyright (C) 2011 Andrew Thomas Ortman <andrew.ortman@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/

//include device code
#include "global.h"
#include "device_rendered_image.cu"
#include "device_rectangle.cu"
#include "device_color.cu"
#include "device_reconimage.cu"
#include "math.h"

__global__ void calculate_generation_fitness(ml_rendered_im_t* target, ml_recon_im_t** members, ml_rendered_im_t** rendered_out, float* fitness_out)
{
	//allocate enough shared memory for the newer GPUs
	__shared__ float segment_fitness_values[1024];
	int member_idx = blockIdx.x;
	
	ml_rendered_im_t* member_rendered_out_g = rendered_out[member_idx];
	ml_recon_im_t* member_recon_g = members[member_idx];
	
	//copy the recon image to shared memory since we access it alot
	__shared__ ml_recon_im_t member_recon;
	if(threadIdx.x == 0 && threadIdx.y == 0) member_recon = *member_recon_g; //just have one of the threads load memory
	__syncthreads(); //wait until that thread finished
	
	//now each image is split up by x/y thread segments
	//bad things will happen if thread dimensions exceed target size
	//Just make sure that the thread dimensions are equal to or less than target size 
	//before passing to kernel. This way we arent wasting any time on each thread determining
	//if it should work or not
	int segment_width = target->width/blockDim.x;
	int segment_height = target->height/blockDim.y;
	int segment_start_x = segment_width*threadIdx.x;
	int segment_end_x = segment_start_x+segment_width;
	int segment_start_y = segment_height*threadIdx.y;
	int segment_end_y = segment_start_y+segment_height;
	
	int x,y;
	float fitness = 0.0f;
	for(x = segment_start_x; x < segment_end_x; x++)
	{
		for(y = segment_start_y; y < segment_end_y; y++)
		{
			ml_color_t rendered_pixel = ml_reconimage_renderpixel_device(&member_recon, x, y);
			ml_renderedimage_setpixel_device(member_rendered_out_g, x, y, rendered_pixel); //for visual debugging really
			
			//calculate the fitness for this pixel
			ml_color_t target_pixel = ml_renderedimage_getpixel_device(target, x, y);
			float red_offset = target_pixel.red - rendered_pixel.red;
			float green_offset = target_pixel.green - rendered_pixel.green;
			float blue_offset = target_pixel.blue - rendered_pixel.blue;
			
			//returns a range from 1 to 0
			//1 means identical pixel
			//0 means complete opposites
			fitness += 1.0f-sqrt((red_offset*red_offset + green_offset*green_offset + blue_offset*blue_offset)/3.0f);
		}
	}
	
	segment_fitness_values[threadIdx.y*blockDim.x + threadIdx.x] = fitness;
	__syncthreads(); //get all of the fitness values for this image
	
	if(threadIdx.x == 0) //combine each row together
	{
		int i = 0;
		for(i = 1; i < blockDim.x; i++)
		{
			segment_fitness_values[threadIdx.y*blockDim.x] += segment_fitness_values[threadIdx.y*blockDim.x + i];
		}
	}
	
	__syncthreads();
	
	if(threadIdx.x == 0 && threadIdx.y == 0) //combine the left column (which are the row sums)
	{
		int i = 0;
		for(i = 1; i < blockDim.y; i++)
		{
			segment_fitness_values[0] += segment_fitness_values[i*blockDim.x];
		}
	}
	//fitness_out[blockIdx.x] = 3;
	fitness_out[blockIdx.x] = segment_fitness_values[0]/(target->width*target->height);
}

void executeFitnessKernel(ml_rendered_im_t* target_device, ml_recon_im_t** recon_list_device, ml_rendered_im_t** rendered_list_device, int size, float* output_host)
{
	dim3 thread_size(32,32);
	dim3 block_size(size);
	
	float* fitness_device;
	HANDLE_CUDA_ERROR(cudaMalloc(&fitness_device, sizeof(float)*size));
	
	calculate_generation_fitness<<<block_size, thread_size>>>(target_device, recon_list_device, rendered_list_device, fitness_device);
	cudaError_t cuda_err;
	if((cuda_err = cudaGetLastError()) != cudaSuccess)
	{
		HANDLE_CUDA_ERROR(cuda_err);
	}
	else
	{
		DEBUG("cuda returned: %s\r\n", cudaGetErrorString(cuda_err));
	}
	
	HANDLE_CUDA_ERROR(cudaMemcpy(output_host, fitness_device, sizeof(float)*size, cudaMemcpyDeviceToHost));
	HANDLE_CUDA_ERROR(cudaFree(fitness_device));
}
