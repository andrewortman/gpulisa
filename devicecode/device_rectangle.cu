/***********************************************************************
* GPU Lisa: Image Reconstruction on the GPU
* Copyright (C) 2011 Andrew Thomas Ortman <andrew.ortman@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/

#ifndef ML_DEVICE_RECTANGLE_SOURCE
#define ML_DEVICE_RECTANGLE_SOURCE

#include "device_rectangle.cuh"

//returns true if a point (x,y) is in bounds of the rectangle (optimized for device)
__host__ __device__ bool ml_rectangle_pointinbounds_device(ml_rectangle_t* rectangle, int x, int y)
{
	return (x > rectangle->x && x < (rectangle->x + rectangle->width)) && (y > rectangle->y && y < (rectangle->y + rectangle->height));
}

#endif