/***********************************************************************
* GPU Lisa: Image Reconstruction on the GPU
* Copyright (C) 2011 Andrew Thomas Ortman <andrew.ortman@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/

#ifndef ML_DEVICE_RENDERED_IMAGE
#define ML_DEVICE_RENDERED_IMAGE

#include "global.h"
#include "color.h"
#include "rendered_image.h"

//sets a pixel on a rendered image (optimized device code)
__host__ __device__ void ml_renderedimage_setpixel_device(ml_rendered_im_t* image, int x, int y, ml_color_t new_color);

//gets a pixel from a rendered image (optimized device code)
__host__ __device__ ml_color_t ml_renderedimage_getpixel_device(ml_rendered_im_t* image, int x, int y);

#endif