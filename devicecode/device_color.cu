/***********************************************************************
* GPU Lisa: Image Reconstruction on the GPU
* Copyright (C) 2011 Andrew Thomas Ortman <andrew.ortman@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/

#ifndef ML_DEVICE_COLOR_DEVICE_SOURCE
#define ML_DEVICE_COLOR_DEVICE_SOURCE

#include "device_color.cuh"

//Performs an alpha composite on an translucent color onto a opaque color.
//Thanks Wikipedia on this one - see 'Alpha Compositing'
__host__ __device__ ml_color_t ml_color_composite_alpha_device(ml_color_alpha_t overlay, ml_color_t background)
{
	ml_color_t output;
	output.red = overlay.color.red*overlay.transp + background.red*(1.0f-overlay.transp);
	output.green = overlay.color.green*overlay.transp + background.green*(1.0f-overlay.transp);
	output.blue = overlay.color.blue*overlay.transp + background.blue*(1.0f-overlay.transp);
	return output;
}

#endif