/***********************************************************************
* GPU Lisa: Image Reconstruction on the GPU
* Copyright (C) 2011 Andrew Thomas Ortman <andrew.ortman@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/

#ifndef ML_DEVICE_RECON_IMAGE_DEVICE
#define ML_DEVICE_RECON_IMAGE_DEVICE

#include "global.h"
#include "recon_image.h"
#include "device_rectangle.cuh"
#include "device_color.cuh"

//flattens a pixel using the recon
__host__ __device__ ml_color_t ml_reconimage_renderpixel_device(ml_recon_im_t* recon, int x, int y);

#endif