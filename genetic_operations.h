/***********************************************************************
* GPU Lisa: Image Reconstruction on the GPU
* Copyright (C) 2011 Andrew Thomas Ortman <andrew.ortman@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/

#ifndef ML_GENTICOPS_H
#define ML_GENTICOPS_H

#include "global.h"
#include "recon_image.h"
#include "rectangle.h"
#include "color.h"

//mutates member (directly changes it in memory)
void ml_geneticops_mutate(unsigned int* random_seed, ml_recon_im_t* member, int target_width, int target_height);

//mutates a a single rectangle
void ml_geneticops_mutate_rectangle(unsigned int* random_seed, ml_rectangle_t* input, int target_width, int target_height);

//mutates a single color
void ml_geneticops_mutate_coloralpha(unsigned int* random_seed, ml_color_alpha_t* input);

#endif