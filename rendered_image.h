/***********************************************************************
* GPU Lisa: Image Reconstruction on the GPU
* Copyright (C) 2011 Andrew Thomas Ortman <andrew.ortman@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/

#ifndef ML_RENDERED_IMAGE
#define ML_RENDERED_IMAGE
#include "global.h"
#include "color.h"
#include "cuda.h"
#include <stdlib.h>

typedef struct
{
	int width, height;
	ml_color_t* pixels;
	
} ml_rendered_im_t;

//host manipulation code:
//sets a pixel on a rendered image (optimized host code)
void ml_renderedimage_setpixel(ml_rendered_im_t* image, int x, int y, ml_color_t new_color);
	
//gets a pixel from a rendered image (optimized host code)
ml_color_t ml_renderedimage_getpixel(ml_rendered_im_t* image, int x, int y);
	
//creates a rendered image in HOST memory
ml_rendered_im_t* ml_renderedimage_create(int width, int height);
	
//frees a rendered image from HOST memory
void ml_renderedimage_free(ml_rendered_im_t* pointer);

//host<->device memory functions:
//allocates a single rendered image (with the pixel array allocated with width*height size)
ml_rendered_im_t* ml_renderedimage_allocatedevice(int width, int height);

//allocates an entire array on the device (with the pixel arrays allocated accordingly)
void ml_renderedimage_allocatearraydevice(ml_rendered_im_t*** host_devicepointer_list_out, ml_rendered_im_t*** device_devicepointer_list_out, int width, int height, int size);

//Downloads a rendered image from the device
void ml_renderedimage_downloadfromdevice(ml_rendered_im_t* host_pointer, ml_rendered_im_t* dev_pointer);
	
//Uploads a rendered image to the GPU (ie, our target image would be uploaded)
ml_rendered_im_t* ml_renderedimage_uploadtodevice(ml_rendered_im_t* host_pointer);
	
//frees a rendered image from DEVICE memory	
void ml_renderedimage_freefromdevice(ml_rendered_im_t* dev_pointer);


#endif