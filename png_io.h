/***********************************************************************
* GPU Lisa: Image Reconstruction on the GPU
* Copyright (C) 2011 Andrew Thomas Ortman <andrew.ortman@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/

#ifndef ML_PNG_IO
#define ML_PNG_IO

#include <png.h>
#include "rendered_image.h"
#include "global.h"


//loads a PNG image into a rendered image object
ml_rendered_im_t* 	ml_png_load(char* filepath);

//writes a rendered image object as a PNG to disk
bool		ml_png_save(char* filepath, ml_rendered_im_t* image);


#endif
