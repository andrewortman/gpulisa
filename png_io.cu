/***********************************************************************
* GPU Lisa: Image Reconstruction on the GPU
* Copyright (C) 2011 Andrew Thomas Ortman <andrew.ortman@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/

#include "png_io.h"

//Loads a PNG image from disk. Returns NULL on failure!
//otherwise returns the image into its raw format as an image_f
ml_rendered_im_t* ml_png_load(char* filepath)
{
	FILE *fp = fopen(filepath , "rb");
	if(!fp)
	{
		DEBUG("File cannot be opened! Try again");
		return NULL;
	}
	
	png_bytep png_signature = (png_bytep)malloc(8);	 			 //png signature is 8 bytes long
	fread(png_signature, 1, 8, fp);					 //read the signature in from the input image
	
	//compare signature with pngsig and make sure it is a png image. png_sig_cmp returns 1 on failure
	if(png_sig_cmp(png_signature, 0, 8)) 
	{
		DEBUG("Does not appear to be a PNG image!");
		return NULL;
	}
	
	DEBUG("PNG Image Validated!");
	
	png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	png_infop info_ptr = png_create_info_struct(png_ptr);
	
	if(!info_ptr)
	{
		png_destroy_read_struct(&png_ptr, NULL, NULL);
		DEBUG("PNG Read Failure.");
		return NULL;
	}
	
	png_init_io(png_ptr, fp);
	
	//we already checked the signature already
	png_set_sig_bytes(png_ptr, 8);
	
	//set up our error handler for libpng - they use jumps so this code will execute in an error
	if(setjmp(png_jmpbuf(png_ptr)))
	{
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		DEBUG("PNG Read Failure.");
		return NULL;
	}
	
	DEBUG("Reading target image.. ");
	//read the PNG
	png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_STRIP_ALPHA | PNG_TRANSFORM_PACKING, NULL);
	
	int height, width;
	height = png_get_image_height(png_ptr, info_ptr);
	width = png_get_image_width(png_ptr, info_ptr);
	fclose(fp);
	
	ml_rendered_im_t* returned_image = ml_renderedimage_create(width, height);
	DEBUG("Success! Size: %i x %i pixels. Converting to ml_rendered_im_t...", width, height);
	
	unsigned char** image_data  = (unsigned char**)png_get_rows(png_ptr, info_ptr);
	
	//fill the image_f variable with the image data.
	int x, y;
	for(y = 0; y < height; y++)
	{
		for(x = 0; x < width; x++)
		{
			float converted_r = image_data[y][x*3] / 255.0f;
			float converted_g = image_data[y][x*3+1] / 255.0f;
			float converted_b = image_data[y][x*3+2] / 255.0f;
			
			ml_color_t converted;
			converted.red = converted_r;
			converted.green = converted_g;
			converted.blue = converted_b;
			
			ml_renderedimage_setpixel(returned_image, x, y, converted);
		}
	}
	
	//clean out unused memory from libpng
	png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
	DEBUG("DONE READING PNG! Total size in memory: %0.3f Mbytes", sizeof(ml_rendered_im_t)+(sizeof(ml_color_t)*height*width)/1048576.0f);
	return returned_image;
}


//Writes the raw format image_f struct to a compressed PNG file.
//Returns true on success, false on failure. 
bool ml_png_save(char* filepath, ml_rendered_im_t* image)
{
	FILE *fp = fopen(filepath , "wb");
	
	if(!fp)
	{
		DEBUG("File cannot be opened for writing!");
		return false;
	}
	
	png_structp png_ptr;
	png_infop info_ptr;
	
	
	//intialize libpng
	png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	info_ptr = png_create_info_struct(png_ptr);
	
	if(!png_ptr)
	{
		DEBUG("Cannot create png! ");
		png_destroy_write_struct(&png_ptr, NULL);
		return false;
	}
	
	if(setjmp(png_jmpbuf(png_ptr)))
	{
		DEBUG("PNG write error.");
		return false;
	}
	
	//setup the io stream
	png_init_io(png_ptr, fp);
	
	//set the headers
	png_set_IHDR(png_ptr, info_ptr, image->width, image->height, 8, 
				PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, 
				PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
	
	//we can write a comment or text here in the future. 
	//look here: http://www.labbookpages.co.uk/software/imgProc/libPNG.html
	
	//write the headers
	png_write_info(png_ptr, info_ptr);
	
	png_bytep row = (png_bytep)malloc(3*image->width * sizeof(png_byte));
	int x,y;
	for(y = 0; y < image->height; y++)
	{
		for(x = 0; x < image->width; x++)
		{
			ml_color_t pixel = ml_renderedimage_getpixel(image, x, y);
			
			row[x*3] = (png_byte)(pixel.red*255);
			row[x*3+1] = (png_byte)(pixel.green*255);
			row[x*3+2] = (png_byte)(pixel.blue*255);
		}
		png_write_row(png_ptr, row);
	}
	
	png_write_end(png_ptr, NULL);
	
	fclose(fp);
	free(row);
	png_destroy_write_struct(&png_ptr, &info_ptr);
	
	DEBUG("DONE WRITING PNG!");
	return true;
}
