/***********************************************************************
* GPU Lisa: Image Reconstruction on the GPU
* Copyright (C) 2011 Andrew Thomas Ortman <andrew.ortman@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/

#include "recon_image.h"

//randomizes a recon_image (both number of rectangles and parameters of each rectangle)
//target width and height refers to the image area during reconstruction (in pixels) which should be the same size
//as the target image
void ml_reconimage_randomize(unsigned int* random_seed, ml_recon_im_t* input, int target_width, int target_height)
{
	if(global_settings.fixed_number_of_rectangles) input->num_rects = MAX_RECTANGLES;
	else input->num_rects = RAND_RI_RANGE(random_seed, 0, MAX_RECTANGLES+1);
	
	for(int i = 0; i < input->num_rects; i++)
	{
		ml_rectangle_randomize(random_seed, &(input->rectangles[i]), target_width, target_height);
	}
}

//adds a rectangle to a reconimage
void ml_reconimage_addrectangle(ml_recon_im_t* recon_image, ml_rectangle_t new_rect, int idx)
{
	if(recon_image->num_rects+1 > MAX_RECTANGLES)
	{
		DEBUG("Maximum number of rectangles reached - cannot add shape.");
		return;
	}
	
	if(idx < 0 || idx > recon_image->num_rects)
	{
		DEBUG("Invalid index number when adding shape. Must be in range of %i -> %i", 0, recon_image->num_rects);
		return;
	}
	
	int i;
	for(i = recon_image->num_rects; i > idx; i--)
	{
		recon_image->rectangles[i] = recon_image->rectangles[i-1];
	}
	
	recon_image->rectangles[idx] = new_rect;
	recon_image->num_rects++;
}

//removes a rectangle from the reconimage
void ml_reconimage_removerectangle(ml_recon_im_t* recon_image, int idx)
{
	if(recon_image->num_rects-1 < 0)
	{
		DEBUG("No rectangles in this image, cannot delete shape");
		return;
	}
	
	if(idx < 0 || idx > recon_image->num_rects)
	{
		DEBUG("Invalid index number when deleting shape. Must be in range of %i -> %i", 0, recon_image->num_rects);
		return;
	}
	
	int i;
	//shift all to left at index to left one, overlapping idx.
	for(i = idx; i < recon_image->num_rects-1; i++)
	{
		recon_image->rectangles[i] = recon_image->rectangles[i+1];
	}
	
	recon_image->num_rects--;
}

//Swaps two rectangles by their index
void ml_reconimage_swaprectangles(ml_recon_im_t* recon_image, int indx1, int indx2)
{
	if(indx1 >= 0 && indx2 >= 0 && indx1 < recon_image->num_rects && indx2 < recon_image->num_rects)
	{
		ml_rectangle_t tmp = recon_image->rectangles[indx1];
		recon_image->rectangles[indx1] = recon_image->rectangles[indx2];
		recon_image->rectangles[indx2] = tmp;
	}
	else
	{
		DEBUG("Could not swap rectangles. Index out of bounds. indx1: %i, indx2: %i, num_rects: %i", indx1, indx2, recon_image->num_rects);
	}
}

//returns a device pointer of the recon_image allocated on the device
ml_recon_im_t* ml_reconimage_allocatedevice()
{
	ml_recon_im_t* to_return;
	HANDLE_CUDA_ERROR(cudaMalloc(&to_return, sizeof(ml_recon_im_t)));
	return to_return;
}

//outputs two pointers:
// host_devicepointer_list_out is a list of device pointers stored in HOST memory. This is useful when overwriting elements without reallocating them on the device
// device_devicepointer_list_out is a pointer to a list of list of device pointers stored in DEVICE memory. 

// example usage:
// ml_recon_im_t* host_device_pointer_list[]; //an array of device pointers for each element of a reconimage array
// ml_recon_im_t** device_device_pointer_list; //device reference for the same pointer list, just residing on device memory
//
// ml_reconimage_allocatearraydevice(&host_device_pointer_list, &device_device_pointer_list, 50);

void ml_reconimage_allocatearraydevice(ml_recon_im_t*** host_devicepointer_list_out, ml_recon_im_t*** device_devicepointer_list_out, int size)
{
	*host_devicepointer_list_out = (ml_recon_im_t**)malloc(sizeof(ml_recon_im_t*)*size);
//	DEBUG("Allocated recon image host pointer list: %p", *host_devicepointer_list_out);
	HANDLE_CUDA_ERROR(cudaMalloc(device_devicepointer_list_out, sizeof(ml_recon_im_t*)*size));
	int i = 0;
	for(i = 0; i < size; i++)
	{
		(*host_devicepointer_list_out)[i] = ml_reconimage_allocatedevice();
//		DEBUG("Allocated recon image pointer %p for element %i", (*host_devicepointer_list_out)[i], i);
	}
	//copy the list we generated over to the device
	HANDLE_CUDA_ERROR(cudaMemcpy(*device_devicepointer_list_out, *host_devicepointer_list_out, sizeof(ml_recon_im_t*)*size, cudaMemcpyHostToDevice));
//	DEBUG("Uploaded recon image devicepointer list to device: HPL:%p, DPL:%p", *host_devicepointer_list_out, *device_devicepointer_list_out);
	
}

//uploads a single image to the device pointer specified
void ml_reconimage_uploadtodevice(ml_recon_im_t* dest_device, ml_recon_im_t* member)
{
//	DEBUG("UPLOADING HOST PTR %p TO DEVICE PTR %p", member, dest_device);
	HANDLE_CUDA_ERROR(cudaMemcpy(dest_device, member, sizeof(ml_recon_im_t), cudaMemcpyHostToDevice));
}

//requires a device pointer list on host memory. uploads elements of 'set' with size 'size' to the device
//uploads each element into memory
//use ml_reconimage_allocatearraydevice in order to create space on the device before any copies take place
//can use this function to overwrite an array already on device since each recon image is of fixed size in memory
void ml_reconimage_uploadarraytodevice(ml_recon_im_t** host_devicepointer_list, ml_recon_im_t* set, int size)
{
	int i;
	for(i = 0; i < size; i++)
	{
		ml_reconimage_uploadtodevice(host_devicepointer_list[i], &(set[i]));
	}
}
