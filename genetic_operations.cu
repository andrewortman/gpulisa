/***********************************************************************
* GPU Lisa: Image Reconstruction on the GPU
* Copyright (C) 2011 Andrew Thomas Ortman <andrew.ortman@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/

#include "genetic_operations.h"

//mutates member (directly changes it in memory)
void ml_geneticops_mutate(unsigned int* random_seed, ml_recon_im_t* member, int target_width, int target_height)
{
	int i;
	for(i = 0; i < member->num_rects; i++)
	{
		ml_geneticops_mutate_rectangle(random_seed, &(member->rectangles[i]), target_width, target_height);
		if(RAND_RB_CHANCE(random_seed, global_settings.swap_rectangles_chance))
		{
			int indx_to_swap_with = 0;
			do
			{
				indx_to_swap_with = RAND_RI_RANGE(random_seed, 0, member->num_rects);
			} while(indx_to_swap_with == i);
			
			ml_reconimage_swaprectangles(member, i, indx_to_swap_with);
			DEBUG("Mutated! Swapped rectangles %i and %i.", i, indx_to_swap_with);
		}
	}
	
	if(!global_settings.fixed_number_of_rectangles) //if we dont have a fixed rectangle count
	{
		if(RAND_RB_CHANCE(random_seed, global_settings.add_rectangle_chance))
		{
			if(member->num_rects < MAX_RECTANGLES)
			{
				ml_rectangle_t new_rectangle;
				ml_rectangle_randomize(random_seed, &new_rectangle, target_width, target_height);
				ml_reconimage_addrectangle(member, new_rectangle, RAND_RI_RANGE(random_seed, 0, member->num_rects));
				DEBUG("Mutated! Added rectangle. New size: %i.", member->num_rects);
			}
		}
	
		if(RAND_RB_CHANCE(random_seed, global_settings.remove_rectangle_chance))
		{
			if(member->num_rects > 0)
			{
				ml_reconimage_removerectangle(member, RAND_RI_RANGE(random_seed, 0, member->num_rects));
				DEBUG("Mutated! Removed rectangle. New size: %i.", member->num_rects);
			}
		}
	}
}

//mutates a single rectangle
void ml_geneticops_mutate_rectangle(unsigned int* random_seed, ml_rectangle_t* input, int target_width, int target_height)
{
	//Mutate the color information
	ml_geneticops_mutate_coloralpha(random_seed, &(input->color_info));
	
	//POSITION
	if(RAND_RB_CHANCE(random_seed, global_settings.change_position_chance))
	{
		int x_old = input->x;
		int x_new = x_old + RAND_RI_RANGE(random_seed, -global_settings.position_max_change, global_settings.position_max_change);
		
		int y_old = input->y;
		int y_new = y_old + RAND_RI_RANGE(random_seed, -global_settings.position_max_change, global_settings.position_max_change);
		
		int x_max = target_width - input->width;
		int y_max = target_height - input->height;
		
		if(x_new > x_max) x_new = x_max;
		else if(y_new < 0) y_new = 0;
		
		if(y_new > y_max) y_new = y_max;
		else if(y_new < 0) y_new = 0;
		
		input->x = x_new;
		input->y = y_new;
		
		DEBUG("Mutated. Position changed from (%i,%i) to (%i,%i)", x_old, y_old, x_new, y_new);
	}
	
	//SCALE
	if(RAND_RB_CHANCE(random_seed, global_settings.change_size_chance))
	{
		int width_old = input->width;
		int width_new = width_old + RAND_RI_RANGE(random_seed, -global_settings.size_max_change, global_settings.size_max_change);
		
		int height_old = input->height;
		int height_new = height_old + RAND_RI_RANGE(random_seed, -global_settings.size_max_change, global_settings.size_max_change);
		
		int width_max = target_width - input->x;
		int height_max = target_height - input->y;
		
		if(width_new > width_max) width_new = width_max;
		else if(width_new < 1) width_new = 1;
		
		if(height_new > height_max) height_new = height_max;
		else if(height_new < 1) height_new = 1;
		
		input->width = width_new;
		input->height = height_new;
		
		DEBUG("Mutated. Size changed from (%i,%i) to (%i,%i)", width_old, height_old, width_new, height_new);
	}
	
	//DONE
}

//mutates a alpha color
void ml_geneticops_mutate_coloralpha(unsigned int* random_seed, ml_color_alpha_t* input)
{
	//RED CHANNEL
	if(RAND_RB_CHANCE(random_seed, global_settings.change_color_chance))
	{
		float old_red = input->color.red;
		float new_red = old_red + RAND_RF_RANGE(random_seed, -global_settings.color_max_change, global_settings.color_max_change);
		
		if(new_red < 0.0f) new_red = 0.0f;
		else if(new_red > 1.0f) new_red = 1.0f;
		
		input->color.red = new_red;
		DEBUG("Mutated. Red channel changed from %f to %f.", old_red, new_red);
	}
	
	//GREEN CHANNEL
	if(RAND_RB_CHANCE(random_seed, global_settings.change_color_chance))
	{
		float old_green = input->color.green;
		float new_green = old_green + RAND_RF_RANGE(random_seed, -global_settings.color_max_change/2, global_settings.color_max_change);
		
		if(new_green < 0.0f) new_green = 0;
		else if(new_green > 1.0f) new_green = 1.0f;
		
		input->color.green = new_green;
		DEBUG("Mutated. Green channel changed from %f to %f.", old_green, new_green);
	}
	
	//BLUE CHANNEL
	if(RAND_RB_CHANCE(random_seed, global_settings.change_color_chance))
	{
		float old_blue = input->color.blue;
		float new_blue = old_blue + RAND_RF_RANGE(random_seed, -global_settings.color_max_change/2, global_settings.color_max_change);
		
		if(new_blue < 0) new_blue = 0;
		else if(new_blue > 1.0f) new_blue = 1.0f;
		
		input->color.blue = new_blue;
		DEBUG("Mutated. Blue channel changed from %f to %f.", old_blue, new_blue);
	}
	
	//TRANSPARENCY
	if(RAND_RB_CHANCE(random_seed, global_settings.change_transparency_chance))
	{
		float old_transp = input->transp;
		float new_transp = old_transp + RAND_RF_RANGE(random_seed, -global_settings.transparency_max_change/2, global_settings.transparency_max_change);
		
		if(new_transp < 0) new_transp = 0;
		else if(new_transp > 1.0f) new_transp = 1.0f;
		
		input->transp = new_transp;
		DEBUG("Mutated. Transparency changed from %f to %f.",old_transp, new_transp);
	}
}