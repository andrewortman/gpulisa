/***********************************************************************
* GPU Lisa: Image Reconstruction on the GPU
* Copyright (C) 2011 Andrew Thomas Ortman <andrew.ortman@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/

#include "rendered_image.h"

//sets a pixel on a rendered image (optimized device code)
//same thing, just a C version for host code
void ml_renderedimage_setpixel(ml_rendered_im_t* image, int x, int y, ml_color_t new_color)
{
	image->pixels[y*image->width + x] = new_color;
}

//gets a pixel from a rendered image (optimized device code)
//same thing, just a C version for host code
ml_color_t ml_renderedimage_getpixel(ml_rendered_im_t* image, int x, int y)
{
	return image->pixels[y*image->width + x];
}

//creates a rendered image in HOST memory
ml_rendered_im_t* ml_renderedimage_create(int width, int height)
{
	ml_rendered_im_t* output = (ml_rendered_im_t*)calloc(1,sizeof(ml_rendered_im_t));
	
	output->width = width;
	output->height = height;
	output->pixels = (ml_color_t*)calloc(1,sizeof(ml_color_t)*width*height);
	return output;
}

//frees a rendered image from HOST memory	
void ml_renderedimage_free(ml_rendered_im_t* pointer)
{
	if(pointer->pixels != NULL)	free(pointer->pixels);
	free(pointer);
}

ml_rendered_im_t* ml_renderedimage_allocatedevice(int width, int height)
{
	ml_rendered_im_t* to_return;
	ml_rendered_im_t temp;
	
	//allocate the pixels and place them into our temporary struct along with the width and height
	temp.width=width;
	temp.height=height;
	HANDLE_CUDA_ERROR(cudaMalloc(&(temp.pixels), sizeof(ml_color_t)*width*height));
	
	//DEBUG("Allocated pixel space (size %lu bytes) at %p", sizeof(ml_color_t)*width*height, temp.pixels);	
	//allocate a spot for the struct
	HANDLE_CUDA_ERROR(cudaMalloc(&to_return, sizeof(ml_rendered_im_t)));
	
	//copy that struct over
	HANDLE_CUDA_ERROR(cudaMemcpy(to_return, &temp, sizeof(ml_rendered_im_t), cudaMemcpyHostToDevice));
	
	return to_return; 
	
}

//creates an array of elements on the device. similar to the reconimage allocate device array function
//returns a host pointer to list of device pointers and a device pointer to a list of device pointers on the device 
void ml_renderedimage_allocatearraydevice(ml_rendered_im_t*** host_devicepointer_list_out, ml_rendered_im_t*** device_devicepointer_list_out, int width, int height, int size)
{
	*host_devicepointer_list_out = (ml_rendered_im_t**)malloc(sizeof(ml_rendered_im_t*)*size);
	//DEBUG("Allocated renderedimage host pointer list: %p", *host_devicepointer_list_out);
	HANDLE_CUDA_ERROR(cudaMalloc(device_devicepointer_list_out, sizeof(ml_rendered_im_t*)*size));
	//DEBUG("Allocated renderedimage device pointer list: %p", *device_devicepointer_list_out);
	int i = 0;
	for(i = 0; i < size; i++)
	{
		(*host_devicepointer_list_out)[i] = ml_renderedimage_allocatedevice(width, height);
		//DEBUG("Allocated rendered image pointer %p for element %i", (*host_devicepointer_list_out)[i], i);
	}
	//copy the list we generated over to the device
	HANDLE_CUDA_ERROR(cudaMemcpy(*device_devicepointer_list_out, *host_devicepointer_list_out, sizeof(ml_rendered_im_t*)*size, cudaMemcpyHostToDevice));
	//DEBUG("Uploaded rendered image devicepointer list to device: HPL:%p, DPL:%p", *host_devicepointer_list_out, *device_devicepointer_list_out);
}

//Downloads a rendered image from the device
//The host rendered image HAS to have been created by the allocation function
void ml_renderedimage_downloadfromdevice(ml_rendered_im_t* host_pointer, ml_rendered_im_t* dev_pointer)
{	
	ml_color_t* host_pixels = host_pointer->pixels;
	//copy the struct and pixels from the device to the host
	HANDLE_CUDA_ERROR(cudaMemcpy(host_pointer, dev_pointer,sizeof(ml_rendered_im_t),cudaMemcpyDeviceToHost));
	HANDLE_CUDA_ERROR(cudaMemcpy(host_pixels, host_pointer->pixels, sizeof(ml_color_t)*host_pointer->width*host_pointer->height, cudaMemcpyDeviceToHost));
		
	host_pointer->pixels = host_pixels;
}

//Uploads a rendered image to the GPU (ie, our target image would be uploaded)
ml_rendered_im_t* ml_renderedimage_uploadtodevice(ml_rendered_im_t* host_pointer)
{
	
	//just manually allocate since this upload only really happens once in the program (target image)
	//we could use allocatedevice but we would have to redownload the struct to figure out where
	//its pixel storage is
	
	ml_rendered_im_t* dev_pointer;
	ml_color_t* pixels;
	//allocate memory on the device first
	HANDLE_CUDA_ERROR(cudaMalloc(&dev_pointer, sizeof(ml_rendered_im_t)));
	HANDLE_CUDA_ERROR(cudaMalloc(&pixels, sizeof(ml_color_t)*host_pointer->width*host_pointer->height));
	
	ml_rendered_im_t copy = *host_pointer; //create a copy of the host pointer to hold the device's pixel pointer
	copy.pixels = pixels; //set the pixel pointer to the device though
	//now copy the pixel data and the rendered image struct over
	HANDLE_CUDA_ERROR(cudaMemcpy(copy.pixels, host_pointer->pixels, sizeof(ml_color_t)*host_pointer->width*host_pointer->height, cudaMemcpyHostToDevice));
	
	HANDLE_CUDA_ERROR(cudaMemcpy(dev_pointer, &copy, sizeof(ml_rendered_im_t), cudaMemcpyHostToDevice));
	
//	DEBUG("Uploaded rendered image to %p", dev_pointer);
	return dev_pointer;
}

//frees a rendered image from DEVICE memory	
void ml_renderedimage_freefromdevice(ml_rendered_im_t* dev_pointer)
{
	//FIXME: Not very efficient since we should know the pixel values anyways.. oh well
	ml_rendered_im_t temp;
	HANDLE_CUDA_ERROR(cudaMemcpy(&temp, dev_pointer, sizeof(ml_rendered_im_t), cudaMemcpyDeviceToHost));
	DEBUG("Freeing pixel memory %p from rendered image %p.", temp.pixels, dev_pointer);
	HANDLE_CUDA_ERROR(cudaFree(temp.pixels)); //free the pixels from the device memory first
	HANDLE_CUDA_ERROR(cudaFree(dev_pointer)); //then remove the rendered image struct from memory
}
