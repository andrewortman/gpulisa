/***********************************************************************
* GPU Lisa: Image Reconstruction on the GPU
* Copyright (C) 2011 Andrew Thomas Ortman <andrew.ortman@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/

#ifndef ML_COLOR
#define ML_COLOR

#include "global.h"

//color is a float since GPU's are optimized for floating point calculations
//this does take a hit on memory though, since we dont really need
//a full 32 bits to represent a channel of a single color.

//but memory really isnt an issue in this application - since the machines we
//are using have plenty of it.
typedef struct 
{
	float red;
	float green;
	float blue;
} ml_color_t;

//ml_color_alpha_t: color+alpha value.
typedef struct
{
	ml_color_t color;
	float transp; //0-> total transparency, 1-> total opacity
} ml_color_alpha_t;

void ml_color_randomize(unsigned int* random_seed, ml_color_t* color);
void ml_color_alpha_randomize(unsigned int* random_seed, ml_color_alpha_t* alpha_color);

#endif