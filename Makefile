# GPULISA Makefile
# Requires: libpng (64bit), openmpi, openmp, and the CUDA SDK
# Compiles on nvcc
# Designed for Compute Core 2.0+ (Fermi GPUs), especially the M20xx series

CC=nvcc
CFLAGS=-m64 -arch=sm_20 -maxrregcount=32 -I ./ -I ./devicecode
LFLAGS=-lpng
TARGET=gpulisa

OBJS=rendered_image.o rectangle.o main.o kernels.o png_io.o recon_image.o color.o genetic_operations.o


debug: CFLAGS += -D__MLDEBUG__ -G -g
debug: build

release: CFLAGS += -O3
release: build

build: $(OBJS)
	$(CC) $(CFLAGS) $(LFLAGS) $(OBJS) -o $(TARGET) 

rebuild_debug: clean debug
rebuild_release: clean release
	
rendered_image.o: rendered_image.cu rendered_image.h
	$(CC) $(CFLAGS) -c rendered_image.cu
	
rectangle.o: rectangle.cu rectangle.h
	$(CC) $(CFLAGS) -c rectangle.cu
	
main.o: main.cu
	$(CC) $(CFLAGS) -c main.cu

kernels.o: kernels.cu
	$(CC) $(CFLAGS) -c kernels.cu

png_io.o: png_io.cu png_io.h
	$(CC) $(CFLAGS) -c png_io.cu

recon_image.o: recon_image.cu recon_image.h
	$(CC) $(CFLAGS) -c recon_image.cu
	
	
color.o: color.cu color.h
	$(CC) $(CFLAGS) -c color.cu
	
genetic_operations.o: genetic_operations.h genetic_operations.cu
	$(CC) $(CFLAGS) -c genetic_operations.cu
	
clean: 
	rm -rf $(OBJS)
	rm -rf $(TARGET)
	rm -rf ./outputs/*.png
