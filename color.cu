/***********************************************************************
* GPU Lisa: Image Reconstruction on the GPU
* Copyright (C) 2011 Andrew Thomas Ortman <andrew.ortman@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/

#include "color.h"

void ml_color_randomize(unsigned int* random_seed, ml_color_t* color)
{
	color->red = RAND_RF(random_seed);
	color->green = RAND_RF(random_seed);
	color->blue = RAND_RF(random_seed);
}

void ml_color_alpha_randomize(unsigned int* random_seed ,ml_color_alpha_t* alpha_color)
{
	ml_color_randomize(random_seed, &(alpha_color->color));
	alpha_color->transp = RAND_RF(random_seed);
}