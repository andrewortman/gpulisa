/***********************************************************************
* GPU Lisa: Image Reconstruction on the GPU
* Copyright (C) 2011 Andrew Thomas Ortman <andrew.ortman@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/

#ifndef ML_RECON_IMAGE
#define ML_RECON_IMAGE

#include "global.h"
#include "rectangle.h"

typedef struct
{
	int num_rects;
	ml_rectangle_t rectangles[MAX_RECTANGLES];
} ml_recon_im_t;


//randomizes a recon_image (both number of rectangles and parameters of each rectangle)
//target width and height refers to the image area during reconstruction (in pixels) which should be the same size
//as the target imagev
void ml_reconimage_randomize(unsigned int* random_seed, ml_recon_im_t* input, int target_width, int target_height);

//adds a rectangle at idx. first shifting everything from idx onward to the right by one spot to make room
//will error if array is at the MAX_RECTANGLE limit
void ml_reconimage_addrectangle(ml_recon_im_t* recon_image, ml_rectangle_t new_rect, int idx);

//removes a rectangle for the reconimage and shifts everything to the right of idx left one spot to fill its space
//will error if array is empty
void ml_reconimage_removerectangle(ml_recon_im_t* recon_image, int idx);

//Swaps two rectangles by their index
void ml_reconimage_swaprectangles(ml_recon_im_t* recon_image, int indx1, int indx2);

//device<->host memory functions
//allocates a single recon image on the device
ml_recon_im_t* ml_reconimage_allocatedevice();

//allocates an entire array on the device. see .cu file for furthur documentation
void ml_reconimage_allocatearraydevice(ml_recon_im_t*** host_devicepointer_list_out, ml_recon_im_t*** device_devicepointer_list_out, int size);

//uploads a reconimage to the device at a given destination
void ml_reconimage_uploadtodevice(ml_recon_im_t* dest_device, ml_recon_im_t* member);

//upload an entire array tothe device (given a host devicepointer list and an array on the host)
void ml_reconimage_uploadarraytodevice(ml_recon_im_t** host_devicepointer_list, ml_recon_im_t* set, int size);
#endif
